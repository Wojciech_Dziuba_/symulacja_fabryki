@startuml
' [1a_4] Wojciech Dziuba (292639), Jakub Gaj(297392), Bartosz Błażejczyk(297375)
hide empty members
hide <<function>> circle

' ==== END of < CONFIGURATION > ====

package "Factory Package" {

	class NodeCollection<Node> << (T,olive) >> {
        - _nodeList : list<Node>
        + NodeCollection(collectionType : Node)
        + addNode(node : Node): void
        + findNodeByID(nodeID : ElementID) : std::list<Node>::iterator
        + findNodeByID(nodeID : ElementID) : std::list<Node>::const_iterator
        + removeNodeByID(nodeID : ElementID): void
        + getNodes() : Node[]

        + begin() : std::list<Node>::iterator
        + end() : std::list<Nodes>::iterator

        + begin() : std::list<Node>::const_iterator
        + end() : std::list<Nodes>::const_iterator

        + cbegin() : std::list<Node>::const_iterator
        + cend() : std::list<Nodes>::const_iterator
	}


	class Factory {
	- _ramps : Ramps
	- _workers : Workers
	- _storehouses: Storehouses
        - removeNodeConections() : void
	+ Factory(ramps : Ramps, workers : Workers, storehouses : Storehouses)
        + isCoherent() : bool

        + addRamp(ramp : Ramp) : void
        + addWorker(worker : Worker) : void
        + addStockhouse(stockhouse : Stockhouse) : void

        + getRampByID(rampID : ElementID) : Ramps::const_iterator
        + getWorkerByID(workerID : ElementID) : Workers::const_iterator
        + getStockhouseByID(stockhouseID : ElementID) : Stockhouses::const_iterator

        + getRamps() : Ramps
        + getWorkers() : Workers
        + getStockhouses() : Stockhouses

        + removeRampByID(rampID : ElementID) : void
        + removeWorkerByID(workerID : ElementID) : void
        + removeStockhouseByID(stockhouseID : ElementID) : void
	}


        class Ramps {
        }

        class Workers {
        }

        class Storehouses {
        }

	Factory *-- Ramps
	Factory *-- Workers
	Factory *-- Storehouses
        Ramps --|> NodeCollection  : <<bind>> <Node -> Ramp>
	Workers --|> NodeCollection  : <<bind>> <Node -> Worker>
	Storehouses --|> NodeCollection  : <<bind>> <Node -> Storehouse>

}
' == END of package "Factory"

package "Report Generation" {

	interface IReportNotifier <<interface>> {
        + {abstract} shouldGenereteReport(time : Time) : bool
	}


	class SpecificTurnsReportNotifier {
        - _specificTurnsReports : std::vector<Time>
        + SpecificTurnsReportNotifier(specificTurnsReports : std::vector<Time>)

        + getSpecificTurnsReports() : std::vector<Time>
        + shouldGenereteReport(time : Time) : bool
	}
	

	class IntervalReportNotifier {
        - _reportInterval : TimeOffset
        + IntervalRportNotifier(reportInterval : TimeOffset)

        + getReportInterval() : TimeOffset
        + shouldGenerateReport(time : Time) : bool
	}


	class Reporting <<function>> {
		generateStructureReport(factory : Factory, reportDestinanion : std::ostream) : void
		generateSimulationTurnReport(factory : Factory, currentTurn : Time, reportDestinanion : std::ostream) : void
	}

	SpecificTurnsReportNotifier ..|> IReportNotifier
	IntervalReportNotifier ..|> IReportNotifier 

}
' == END of package "Report Generation"


class Simulation <<function>> {
	simulate(networkStructure : std::istream, simulationTurns : Time, reportDestinanion : std::ostream, reportTimes : IReportNotifier)
}


Simulation ..> Factory : <<uses>>
Reporting <.. Simulation : <<uses>>
IReportNotifier <.. Simulation : <<uses>>


package "Storage" {

	interface IPackageStockpile <<interface>> {
        + {abstract} addPackage(package : Package) : void
        + {abstract} getPackage(packageID : ElementID) : std::deque<Package>::const_iterator

        + {abstract} begin() : std::deque<Package>::const_iterator
        + {abstract} end() : std::deque<Package>::const_iterator
        + {abstract} cbegin() : std::deque<Package>::const_iterator
        + {abstract} cend() : std::deque<Package>::const_iterator
	} 

	interface IPackageQueue <<interface>> {
        + {abstract} removePackage(packageID : ElementID) : void
        + {abstract} popPackage() : std::unique_ptr<Package>
        + {abstract} identyfyQueue() : QueueType
	} 

	enum QueueType <<enumeration>> {
        FIFO
        LIFO
	}

	class PackageQueue {
        - _packageQueue : std::deque<Package>
        - _queueType : QueueType
        + PackageQueue(queueType : QueueType)

        + addPackage(package : Package) : void
        + getPackage(packageID : ElementID) : std::deque<Package>::const_iterator
        + removePackage(packageID : ElementID) : void
        + popPackage() : std::unique_ptr<Package>
        + identyfyQueue() : QueueType

        + begin() : std::deque<Package>::const_iterator
        + end() : std::deque<Package>::const_iterator
        + cbegin() : std::deque<Package>::const_iterator
        + cend() : std::deque<Package>::const_iterator
	}

	class Package {
        - _packageID : ElementID
        - {static} _maxConferedID : int
        + Package(packageID : ElementID)

        + getPackageID() : ElementID
	}

	PackageQueue ..|> IPackageQueue
	QueueType <.. IPackageQueue : <<uses>>
	IPackageQueue --|> IPackageStockpile
        Package --*PackageQueue
}
' == END of package "Storage"


package "Nodes" {

	interface IPackageReceiver <<interface>> {
        + {abstract} identyfyReciever() : std::pair<ElementID, RecieverType>
	+ {abstract} receivePackage(std::unique_ptr<Package> packagePtr): void

	+ {abstract} begin() : std::deque<Package>::const_iterator
        + {abstract} end() : std::deque<Package>::const_iterator
        + {abstract} cbegin() : std:deque<Package>::const_iterator
        + {abstract} cend() : std::deque<Package>::const_iterator
        }

	enum ReceiverType <<enumeration>> {
        Storehouse
        Worker
	}

	class ReceiverPreferences {
        - _recieverPreferences : std::map<std::unique_ptr<IPackageReciever>, ProbabilityGenerator>
        - regenerateProbability(recieverPreferences : std::map<std::unique_ptr<IPackageReciever>, ProbabilityGenerator>) : void

        + RecieverPreferences(recieverPreferences : std::map<std::unique_ptr<IPackageReciever>, ProbabilityGenerator>)

        + addReciecer(reciever_id : ElementID) : void
        + removeReciever(reciever_id : ElementID) : void
        + selectReciever() : std::unique_ptr<IPackageReciever>

        + begin() : std::map<std::unique_ptr<IPackageReciever>, ProbabilityGenerator>::const_iterator
        + end() : std::map<std::unique_ptr<IPackageReciever>, ProbabilityGenerator>::const_iterator
        + cbegin() : std::map<std::unique_ptr<IPackageReciever>, ProbabilityGenerator>::const_iterator
        + cend() : std::map<std::unique_ptr<IPackageReciever>, ProbabilityGenerator>::const_iterator
	}


	class PackageSender {
        - _packageToSendBuffer : std::optional<Package>
        + recieverPreferences : RecieverPreferences
        + {abstract} PackageSender(recieverPreference : RecieverPreferences)

        + sendPackage() : void
        + getPackageToSendBuffer() : std::optional<Package>
        + setPackageToSendBuffer(packageToSend : std::optional<Package>) : void 
	}


	class Ramp {
        - _rampID : ElementID
        - _deliveryTimes : TimeOffset
        + Ramp(rampID : ElementID, delieveryTimes : TimeOffset, recieverPreference : RecieverPreferences)

        + getRampID() : ElementID
        + getDeliveryTimes() : TimeOffset
	}


	class Worker {
        - _workerID : ElemntID
        - _workTime : Time
        - _workBuffer : std::optional<Package>
        - _workQueue : std::unique_ptr<IPackageQueue>
        + Worker(workerID : ElementID, workTime : Time, workQueue : std::unique_ptr<IPackageQueue>, recieverPreference : RecieverPreferences)

        + getWorkerID() : ElemntID
        + getWorkTime() : Time
        + getWorkBuffer() : std::optional<Package>
        + getWorkQueue() : std::unique_ptr<IPackageQueue>
        + doWork() : void

        + identyfyReciever() : std::pair<ElementID, RecieverType>
	+ receivePackage(std::unique_ptr<Package> packagePtr): void
	}


	class Storehouse {
        - _storehouseID : ElementID 
        - _packageQueue : std::unique_ptr<IPackageStockpile>
        + Storehouse(storehouseID : ElementID, packageQueue : std::unique_ptr<IPackageStockpile>)

        + getStorehouseID() : ElementID
        + identyfyReciever() : std::pair<ElementID, RecieverType>
	+ receivePackage(std::unique_ptr<Package> packagePtr): void
	}

	IPackageReceiver ..> ReceiverType : <<uses>>
        PackageSender *-- ReceiverPreferences
	Ramp --|> PackageSender
	Worker --|> PackageSender
	Worker ..|> IPackageReceiver
	IPackageQueue --* Worker
	Storehouse ..|> IPackageReceiver
	IPackageStockpile --* Storehouse

	Ramp -[hidden]> Worker
	Worker -[hidden]> Storehouse

	' == POSITIONING ==
	PackageSender -[hidden]> IPackageReceiver
	' == END of POSITIONING ==
}
' == END of package "Nodes"


' .. HELPERS ----

' .. DATA TYPES --
together {
	class Time << (T,orchid) primitive>>
	class TimeOffset << (T,orchid) primitive>>
	class ElementID << (T,orchid) primitive>>
	class ProbabilityGenerator << (T,orchid) primitive>>

	Time -[hidden]> TimeOffset
	TimeOffset -[hidden]> ElementID
	ElementID -[hidden]> ProbabilityGenerator
}

class IO <<function>> {
	loadFactoryStructure(@TODO)
	saveFactoryStructure(@TODO)
}

' == END of together


' ==== < POSITIONING > ====

IO <-[hidden]- IntervalReportNotifier
Time <-[hidden]- Package

ReceiverPreferences <-[hidden]- IPackageStockpile

IPackageStockpile -[hidden]-> Storehouse

"Factory Package" +- "Nodes"

IReportNotifier -[hidden]> Reporting

' ==== END of < POSITIONING > ====
' [1a_4] Wojciech Dziuba (292639), Jakub Gaj(297392), Bartosz Błażejczyk(297375)
@enduml