//
// Created by Wojciech Dziuba (292639) on 06.01.2019.
//


#ifndef NETWORK_SPECIFICTURNSREPORTNOTIFIER_HPP
#define NETWORK_SPECIFICTURNSREPORTNOTIFIER_HPP

#include <vector>
#include "IReportNotifier.hpp"

class SpecificTurnsReportNotifier : public IReportNotifier {
private:
    std::vector<Time> _specificTurnsReports;

public:
    SpecificTurnsReportNotifier(std::vector<Time>&& specificTurnsReports)
    : _specificTurnsReports(std::move(specificTurnsReports)) {

    }

    std::vector<Time> getSpecificTurnsReports() const {}
    bool shouldGenerateReport(const Time time) {}
};


#endif //NETWORK_SPECIFICTURNSREPORTNOTIFIER_HPP
