//
// Created by Wojciech Dziuba (292639) on 06.01.2019.
//


#ifndef NETWORK_INTERVALREPORTNOTIFIER_HPP
#define NETWORK_INTERVALREPORTNOTIFIER_HPP

#include "../Globals/Globals.hpp"
#include "IReportNotifier.hpp"

class IntervalReportNotifier : public IReportNotifier {
private:
    TimeOffset _reportInterval;

public:
    explicit IntervalReportNotifier(TimeOffset reportInterval)
    : _reportInterval(reportInterval) {

    }

    TimeOffset getReportInterval() const {}
    bool shouldGenerateReport(const Time& time) const override {}
};


#endif //NETWORK_INTERVALREPORTNOTIFIER_HPP
