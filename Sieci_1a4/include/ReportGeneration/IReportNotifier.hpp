//
// Created by Wojciech Dziuba (292639) on 06.01.2019.
//


#ifndef NETWORK_IREPORTNOTIFIER_HPP
#define NETWORK_IREPORTNOTIFIER_HPP


#include "../Globals/Globals.hpp"

class IReportNotifier {
public:
    virtual bool shouldGenerateReport(const Time& time) const = 0;
    virtual ~IReportNotifier() = default;
};


#endif //NETWORK_IREPORTNOTIFIER_HPP
