//
// Created by Wojciech Dziuba (292639) on 06.01.2019.
//


#ifndef NETWORK_REPORTING_HPP
#define NETWORK_REPORTING_HPP

#include "../Globals/Globals.hpp"
#include "../FactoryPackage/Factory.hpp"

void generateStructureReport(const Factory& factory, std::ostream reportDestination){

}

void generateSimulationTurnReport(const Factory& factory, Time currentTurn, std::ostream reportDestination){

}


#endif //NETWORK_REPORTING_HPP
