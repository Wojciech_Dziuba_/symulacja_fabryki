//
// Created by Wojciech Dziuba (292639) on 06.01.2019.
//


#ifndef NETWORK_RECIEVERPREFERENCES_HPP
#define NETWORK_RECIEVERPREFERENCES_HPP

#include "../Globals/Globals.hpp"
#include "IPackageReceiver.hpp"


class ReceiverPreferences {
private:
    std::map<IPackageReceiver*, ProbabilityGenerator> _receiverPreferences;
    void scaleProbability(std::map<IPackageReceiver*, ProbabilityGenerator> receiverPreferences);

public:
    explicit ReceiverPreferences(std::map<IPackageReceiver*, ProbabilityGenerator>& receiverPreferences);

    void addReceiver(IPackageReceiver* newReceiver, ProbabilityGenerator probability);
    void removeReceiver(const ElementID& receiverID);
    IPackageReceiver* selectReceiver(std::function<double()> function) const;

    std::map<IPackageReceiver*, ProbabilityGenerator>::const_iterator begin() const;
    std::map<IPackageReceiver*, ProbabilityGenerator>::const_iterator end() const;
    std::map<IPackageReceiver*, ProbabilityGenerator>::const_iterator cbegin() const;
    std::map<IPackageReceiver*, ProbabilityGenerator>::const_iterator cend() const;
};

double randDouble() ;
double choose_number_from_0_to_1(double number);
double get_0_5();

#endif //NETWORK_RECIEVERPREFERENCES_HPP
