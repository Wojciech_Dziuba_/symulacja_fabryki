//
// Created by Wojciech Dziuba (292639) on 06.01.2019.
//


#ifndef NETWORK_IPACKAGERECIEVER_HPP
#define NETWORK_IPACKAGERECIEVER_HPP

#include "../Globals/Globals.hpp"
#include "ReceiverType.hpp"
#include "../Storage/Package.hpp"


class IPackageReceiver {
public:
    virtual std::pair<ReceiverType , ElementID > identifyReceiver() const = 0;
    virtual void receivePackage(std::unique_ptr<Package>& packagePtr) const = 0;

    virtual std::deque<Package>::const_iterator begin() const  = 0;
    virtual std::deque<Package>::const_iterator end() const  = 0;
    virtual std::deque<Package>::const_iterator cbegin() const = 0;
    virtual std::deque<Package>::const_iterator cend() const = 0;
    virtual ~IPackageReceiver() = default;
};


#endif //NETWORK_IPACKAGERECIEVER_HPP
