//
// Created by Wojciech Dziuba (292639) on 06.01.2019.
//


#ifndef NETWORK_WORKER_HPP
#define NETWORK_WORKER_HPP


#include "../Globals/Globals.hpp"
#include "../Storage/Package.hpp"
#include "../Storage/IPackageQueue.hpp"
#include "ReceiverPreferences.hpp"
#include "PackageSender.hpp"

class Worker : public IPackageReceiver, public PackageSender{
private:
    ElementID _workerID;
    TimeOffset _workTime;
    IPackageQueue* _workQueue;
    Time _currentWorkTurn;
    std::optional<Package> _workBuffer;


public:
    Worker(ElementID workerID, TimeOffset workTime, IPackageQueue* workQueue, ReceiverPreferences& preferences);

    IPackageQueue* getWorkQueue() const;

    std::optional<Package> getWorkBuffer() const;
    
    ElementID getID() const;

    void doWork();

    void receivePackage(std::unique_ptr<Package>& packagePtr) const override;
    std::pair<ReceiverType, ElementID > identifyReceiver() const override;

    std::deque<Package>::const_iterator begin() const override;
    std::deque<Package>::const_iterator end() const override;
    std::deque<Package>::const_iterator cbegin() const override;
    std::deque<Package>::const_iterator cend() const override;
};


#endif //NETWORK_WORKER_HPP
