//
// Created by Wojciech Dziuba (292639) on 06.01.2019.
//

#ifndef NETWORK_PACKAGESENDER_HPP
#define NETWORK_PACKAGESENDER_HPP

#include "../Globals/Globals.hpp"
#include "../Storage/Package.hpp"
#include "ReceiverPreferences.hpp"

class PackageSender {
private:
    std::optional<Package> _packageToSendBuffer;

public:
    ReceiverPreferences receiverPreferences;
    PackageSender(ReceiverPreferences& preferences);
    void sendPackage();
    std::optional<Package> getPackageToSendBuffer() const;
    void setPackageToSendBuffer(const std::optional<Package>& packageToSend);
};


#endif //NETWORK_PACKAGESENDER_HPP
