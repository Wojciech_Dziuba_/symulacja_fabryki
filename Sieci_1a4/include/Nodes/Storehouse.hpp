//
// Created by Wojciech Dziuba (292639) on 06.01.2019.
//


#ifndef NETWORK_STOREHOUSE_HPP
#define NETWORK_STOREHOUSE_HPP

#include <QueueType.hpp>
#include "../Globals/Globals.hpp"
#include "IPackageReceiver.hpp"
#include "../Storage/IPackageStockpile.hpp"

class Storehouse : public IPackageReceiver{
private:
    ElementID _storehouseID;
    IPackageStockpile* _packageQueue;
public:
    Storehouse(ElementID storehouseID, IPackageStockpile* packageQueue);

    ElementID getID() const;

    std::pair<ReceiverType , ElementID > identifyReceiver() const override;
    void receivePackage(std::unique_ptr<Package>& packagePtr) const override;

    std::deque<Package>::const_iterator begin() const override;
    std::deque<Package>::const_iterator end() const override;
    std::deque<Package>::const_iterator cbegin() const override;
    std::deque<Package>::const_iterator cend() const override;
};


#endif //NETWORK_STOREHOUSE_HPP
