//
// Created by Wojciech Dziuba (292639) on 06.01.2019.
//


#ifndef NETWORK_RECEIVERTYPE_HPP
#define NETWORK_RECEIVERTYPE_HPP


enum class ReceiverType {
    Storehouse,
    Worker
};


#endif //NETWORK_RECEIVERTYPE_HPP
