//
// Created by Wojciech Dziuba (292639) on 06.01.2019.
//


#ifndef NETWORK_RAMP_HPP
#define NETWORK_RAMP_HPP

#include <Globals.hpp>
#include <ReceiverPreferences.hpp>
#include <PackageSender.hpp>
#include <Package.hpp>

class Ramp : public PackageSender {
private:
    ElementID _rampID;
    TimeOffset _delieveryTimes;

public:
    Ramp(ElementID rampID, TimeOffset delieveryTimes, ReceiverPreferences& preferences);

    ElementID getID() const;
    TimeOffset getDeliveryTimes() const;

    void tryGeneratePackage(const Time& currentTurn);
};


#endif //NETWORK_RAMP_H
