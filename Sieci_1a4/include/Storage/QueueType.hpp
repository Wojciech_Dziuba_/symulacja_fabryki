//OK
// Created by wojte on 06.01.2019.
//

#ifndef NETWORK_QUEUETYPE_HPP
#define NETWORK_QUEUETYPE_HPP


enum class QueueType {
    FIFO,
    LIFO
};


#endif //NETWORK_QUEUETYPE_HPP
