//OK
// Created by wojte on 06.01.2019.
//

#ifndef NETWORK_PACKAGE_HPP
#define NETWORK_PACKAGE_HPP


#include "../Globals/Globals.hpp"

static ElementID id_counter=1;

class Package {
private:
    static int _maxConferedID;
    ElementID _packageID;

public:
    explicit Package();


    ElementID getPackageID() const;
};


#endif //NETWORK_PACKAGE_HPP
