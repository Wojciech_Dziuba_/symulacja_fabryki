//OK
// Created by Wojciech Dziuba on 06.01.2019.
//

#ifndef NETWORK_PACKAGEQUEUE_HPP
#define NETWORK_PACKAGEQUEUE_HPP

#include "../Globals/Globals.hpp"
#include "Package.hpp"
#include "QueueType.hpp"
#include "IPackageQueue.hpp"

class PackageQueue : public IPackageQueue {
private:
    std::deque<Package> _packageQueue;
    QueueType _queueType;

public:
    PackageQueue(QueueType queueType);

    void addPackage(std::unique_ptr<Package> package) override;
    std::deque<Package>::const_iterator getPackage(const ElementID& packageID) const override;
    void removePackage(ElementID packageID) override;
    std::unique_ptr<Package> popPackage() override;
    QueueType identifyQueue() const override;


    std::deque<Package>::const_iterator begin()const override;
    std::deque<Package>::const_iterator end() const override;
    std::deque<Package>::const_iterator cbegin() const override;
    std::deque<Package>::const_iterator cend() const override;


};


#endif //NETWORK_PACKAGEQUEUE_HPP

