//OK
// Created by wojte on 06.01.2019.
//

#ifndef NETWORK_IPACKAGEQUEUE_HPP
#define NETWORK_IPACKAGEQUEUE_HPP

#include "../Globals/Globals.hpp"
#include "Package.hpp"
#include "QueueType.hpp"
#include "IPackageStockpile.hpp"

class IPackageQueue : public IPackageStockpile {
public:
    virtual void removePackage(ElementID packageID) = 0;
    virtual std::unique_ptr<Package> popPackage() = 0;
    virtual QueueType identifyQueue() const = 0;

    virtual ~IPackageQueue() = default;

};


#endif //NETWORK_IPACKAGEQUEUE_HPP
