//OK
// Created by wojte on 06.01.2019.
//

#ifndef NETWORK_IPACKAGESTOCKPILE_HPP
#define NETWORK_IPACKAGESTOCKPILE_HPP


#include "../Globals/Globals.hpp"
#include "Package.hpp"

class IPackageStockpile {
public:
    virtual void addPackage(std::unique_ptr<Package> package) = 0;
    virtual std::deque<Package>::const_iterator getPackage(const ElementID& packageID) const = 0;

    virtual std::deque<Package>::const_iterator begin() const  = 0;
    virtual std::deque<Package>::const_iterator end() const = 0;
    virtual std::deque<Package>::const_iterator cbegin() const = 0;
    virtual std::deque<Package>::const_iterator cend() const = 0;

    virtual ~IPackageStockpile() = default;
};


#endif //NETWORK_IPACKAGESTOCKPILE_HPP
