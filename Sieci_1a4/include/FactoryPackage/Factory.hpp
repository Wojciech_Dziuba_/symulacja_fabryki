//
// Created by Wojciech Dziuba (292639) on 06.01.2019.
//

#ifndef NETWORK_FACTORY_HPP
#define NETWORK_FACTORY_HPP

#include "NodeCollection.hpp"
#include "ReceiverPreferences.hpp"
#include "Ramp.hpp"
#include "Worker.hpp"
#include "Storehouse.hpp"

class Factory {
private:
    Ramps _ramps;
    Workers _workers;
    Storehouses _storehouses;

    void removeNodeConnections(ElementID nodeID);

public:
    Factory(Ramps& ramps, Workers& workers, Storehouses& storehouses);

    bool isCoherent();

    void addRamp(Ramp ramp);
    void addWorker(Worker worker);
    void addStorehouse(Storehouse storehouse);

    Ramps::iterator getRampByID(ElementID rampID);
    Ramps::const_iterator getRampByID(ElementID rampID) const;
    Workers::iterator getWorkerByID(ElementID workerID) ;
    Workers::const_iterator getWorkerByID(ElementID workerID) const;
    Storehouses::iterator getStorehouseByID(ElementID storehouseID);
    Storehouses::const_iterator getStorehouseByID(ElementID storehouseID) const;

    Ramps getRamps();
    Workers getWorkers();
    Storehouses getStorehouses();

    void removeRampByID(ElementID rampID);
    void removeWorkerByID(ElementID workerID);
    void removeStorehouseByID(ElementID storehouseID);


};


#endif //NETWORK_FACTORY_HPP
