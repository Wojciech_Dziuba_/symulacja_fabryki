//
// Created by Wojciech Dziuba (292639) on 06.01.2019.
//

#ifndef NETWORK_NODECOLLECTION_HPP
#define NETWORK_NODECOLLECTION_HPP


#include "../Globals/Globals.hpp"
#include "../Nodes/Ramp.hpp"
#include "../Nodes/Storehouse.hpp"
#include "../Nodes/Worker.hpp"


template <class Node> class NodeCollection {
private:
    std::list<Node> _nodeList;

public:
    using const_iterator = typename std::list<Node>::const_iterator;
    using iterator = typename std::list<Node>::iterator;

    NodeCollection(std::list<Node>& nodeList) : _nodeList(nodeList) {}

    void addNode(Node node) { _nodeList.push_back(node); }

    typename std::list<Node>::iterator getNodeByID(ElementID nodeID) {
        auto it = std::find_if(_nodeList.begin(), _nodeList.end(), [nodeID](const Node& node){ return (node.getID() == nodeID); });
        return it;
    }
    typename std::list<Node>::const_iterator getNodeByID(const ElementID nodeID) const {
        auto it = std::find_if(_nodeList.begin(), _nodeList.end(), [nodeID](const Node& node){ return (node.getID() == nodeID); });
        return it;
    }

    void removeNodeByID(ElementID nodeID) {
        if(getNodeByID(nodeID) != _nodeList.end()) {
            _nodeList.erase(getNodeByID(nodeID));
        }
    }

    Node getNodes() const {
        return _nodeList;
    }

    typename std::list<Node>::iterator begin() { return _nodeList.begin(); }
    typename std::list<Node>::iterator end() { return _nodeList.end(); }

    typename std::list<Node>::const_iterator begin() const { return _nodeList.cbegin(); }
    typename std::list<Node>::const_iterator end() const { return _nodeList.cend(); }

    typename std::list<Node>::const_iterator cbegin() const { return _nodeList.cbegin(); }
    typename std::list<Node>::const_iterator cend() const { return _nodeList.cend(); }
};

using Ramps = NodeCollection<Ramp>;
using Workers = NodeCollection<Worker> ;
using Storehouses = NodeCollection<Storehouse>;


#endif //NETWORK_NODECOLLECTION_HPP
