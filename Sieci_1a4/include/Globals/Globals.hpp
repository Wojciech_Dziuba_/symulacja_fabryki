//
// Created by Wojciech Dziuba (292639) on 06.01.2019.
//


#ifndef NETWORK_GLOBALS_HPP
#define NETWORK_GLOBALS_HPP

#include <iostream>
#include <string>
#include <list>
#include <deque>
#include <memory>
#include <functional>
#include <map>


using ElementID = int;
using TimeOffset = long;
using Time = long;
using ProbabilityGenerator = double; // tymczasowo!!!


#endif //NETWORK_GLOBALS_HPP
