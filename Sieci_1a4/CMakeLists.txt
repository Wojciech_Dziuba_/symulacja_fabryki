cmake_minimum_required(VERSION 3.13)
project(Sieci_1a4)

set(CMAKE_CXX_STANDARD 17)

#add_compile_options(-Wall -Wextra -Werror -Wpedantic -pedantic-errors)

include_directories(
        include/Nodes
        include/Globals
        include/Storage
        include/FactoryPackage
        googletest-master/googletest/include
        googletest-master/googlemock/include)


#współdzielone pliki .cpp
set(SOURCE_FILES
        src/FactoryPackage/Factory.cpp
        src/Nodes/PackageSender.cpp
        src/Nodes/Ramp.cpp
        src/Nodes/ReceiverPreferences.cpp
        src/Nodes/Storehouse.cpp
        src/Nodes/Worker.cpp
        src/Storage/Package.cpp
        src/Storage/PackageQueue.cpp
        )

#pliki .cpp dla testow
set(TEST_SOURCES
        test/main_test.cpp      #plik z main() dla testów
        )

set(TEST_SOURCES_NODES
        test/ramp_test.cpp
        test/worker_test.cpp
        test/storehouse_test.cpp
        test/receiver_preferences_test.cpp
        test/package_sender_test.cpp
        )

set(TEST_SOURCES_FACTORYPACKAGE
        test/factory_test.cpp
        test/node_collection_test.cpp
        )

set(TEST_SOURCES_STORAGE
        test/storage_test.cpp)

# tutaj dodajemy lib Google Test i kompiluje
# za pomocą CMakeLists.txt z folderu googletest-master
add_subdirectory(googletest-master)
link_libraries(gmock)

add_executable( sieci_run ${SOURCE_FILES} src/siec.cpp )

add_executable( sieci_test ${SOURCE_FILES} ${TEST_SOURCES} )

add_executable( nodes_test ${SOURCE_FILES} ${TEST_SOURCES} ${TEST_SOURCES_NODES} )

add_executable( storage_test ${SOURCE_FILES} ${TEST_SOURCES} ${TEST_SOURCES_STORAGE} )

add_executable( factory_package_test ${SOURCE_FILES} ${TEST_SOURCES} ${TEST_SOURCES_FACTORYPACKAGE})

# Temporary executables
 add_executable(REEEEEEEEEEEEEEEEEE test/node_collection_test.cpp test/main_test.cpp)
 add_executable(FAAAACTOOORRYYY test/factory_test.cpp test/main_test.cpp)
