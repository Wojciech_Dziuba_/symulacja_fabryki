//
// Created by Kubq on 2019-01-12.
//

#include <PackageQueue.hpp>

PackageQueue::PackageQueue(QueueType queueType)
: _queueType(queueType) {}

void PackageQueue::addPackage(std::unique_ptr<Package> package){
    _packageQueue.push_back(*std::move(package));
}

std::deque<Package>::const_iterator PackageQueue::getPackage(const ElementID& packageID) const {
    for(auto ele = cbegin();ele!=cend();++ele){
        if(ele->getPackageID()==packageID){return ele;}
    }
}

void PackageQueue::removePackage(const ElementID packageID) {
    _packageQueue.erase(getPackage(packageID));
}

std::unique_ptr<Package> PackageQueue::popPackage() {
    if (_queueType == QueueType::FIFO) {
        std::unique_ptr<Package> packagePtr = std::make_unique<Package>(_packageQueue.front());
        _packageQueue.pop_front();
        return packagePtr;
    } else if (_queueType == QueueType::LIFO) {
        std::unique_ptr<Package> packagePtr = std::make_unique<Package>(_packageQueue.back());
        _packageQueue.pop_back();
        return packagePtr;
    }
}

QueueType PackageQueue::identifyQueue() const {return _queueType;}


std::deque<Package>::const_iterator PackageQueue::begin() const { return _packageQueue.begin(); }
std::deque<Package>::const_iterator PackageQueue::end() const { return _packageQueue.end(); }
std::deque<Package>::const_iterator PackageQueue::cbegin() const { return _packageQueue.cbegin(); }
std::deque<Package>::const_iterator PackageQueue::cend() const { return _packageQueue.cend(); }


