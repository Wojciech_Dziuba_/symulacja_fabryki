//
// Created by Wojciech Dziuba (292639) on 19.01.2019.
//
#include <Factory.hpp>

Factory::Factory(Ramps& ramps, Workers& workers, Storehouses& storehouses) : _ramps(ramps), _workers(workers), _storehouses(storehouses) {}

void Factory::addRamp(Ramp ramp) { _ramps.addNode(std::move(ramp)); }
void Factory::addWorker(Worker worker) { _workers.addNode(std::move(worker)); }
void Factory::addStorehouse(Storehouse storehouse) { _storehouses.addNode(std::move(storehouse)); }

Ramps::iterator Factory::getRampByID(ElementID rampID) {
    return _ramps.getNodeByID(rampID);
}
Ramps::const_iterator Factory::getRampByID(ElementID rampID) const {
    return _ramps.getNodeByID(rampID);
}

Workers::iterator Factory::getWorkerByID(ElementID workerID) {
    return _workers.getNodeByID(workerID);
}
Workers::const_iterator Factory::getWorkerByID(ElementID workerID) const {
    return _workers.getNodeByID(workerID);
}

Storehouses::iterator Factory::getStorehouseByID(ElementID storehouseID) {
        return _storehouses.getNodeByID(storehouseID);
}
Storehouses::const_iterator Factory::getStorehouseByID(ElementID storehouseID) const {
    return _storehouses.getNodeByID(storehouseID);
}

Ramps Factory::getRamps() { return _ramps; }
Workers Factory::getWorkers() { return _workers; }
Storehouses Factory::getStorehouses() { return _storehouses; }

void Factory::removeRampByID(ElementID rampID) {
    removeNodeConnections(rampID);
    _ramps.removeNodeByID(rampID);
}
void Factory::removeWorkerByID(ElementID workerID) {
    removeNodeConnections(workerID);
    _workers.removeNodeByID(workerID);
}
void Factory::removeStorehouseByID(ElementID storehouseID) {
    removeNodeConnections(storehouseID);
    _storehouses.removeNodeByID(storehouseID);
}

bool Factory::isCoherent() {
    for(const Ramp& ramp  : _ramps){
        int number_of_recievers = 0;
        for(const auto receiver : ramp.receiverPreferences) {
            if (receiver.first->identifyReceiver().first == ReceiverType::Worker || receiver.first->identifyReceiver().first == ReceiverType::Storehouse){
                number_of_recievers++;
            }
        }
        if(number_of_recievers <= 0) { return false; }
    }

    for(const Worker& worker : _workers){
        int number_of_receivers = 0;
        for(const auto receiver : worker.receiverPreferences){
            if ( receiver.first->identifyReceiver().first == ReceiverType::Worker || receiver.first->identifyReceiver().first == ReceiverType::Storehouse)
                number_of_receivers++;
        }
        if(number_of_receivers <= 0) { return false; }
    }
    return true;
}


void Factory::removeNodeConnections(ElementID nodeID) {
    for(Ramp &ramp : _ramps){
        ramp.receiverPreferences.removeReceiver(nodeID);
    }
    for(Worker &worker : _workers){
        worker.receiverPreferences.removeReceiver(nodeID);
    }
} // Nie działa wbudowane usuwanie odbiorców z klasy RecieverPreferences

