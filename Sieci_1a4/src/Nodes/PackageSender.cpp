//
// Created by Kubq on 2019-01-13.
//

#include <PackageSender.hpp>
#include <ReceiverPreferences.hpp>

PackageSender::PackageSender(ReceiverPreferences &preferences) : receiverPreferences(std::move(preferences)){}

void PackageSender::sendPackage() {
    if(_packageToSendBuffer){
        std::function<double()> fun=randDouble;
        IPackageReceiver* selected=receiverPreferences.selectReceiver(fun);
        std::unique_ptr<Package> pack_ptr = std::make_unique<Package> (_packageToSendBuffer.value());
        selected->receivePackage(pack_ptr);
        _packageToSendBuffer.reset();
    }
}

std::optional<Package> PackageSender::getPackageToSendBuffer() const {return _packageToSendBuffer;}

void PackageSender::setPackageToSendBuffer(const std::optional<Package>& packageToSend) {_packageToSendBuffer=packageToSend;}

