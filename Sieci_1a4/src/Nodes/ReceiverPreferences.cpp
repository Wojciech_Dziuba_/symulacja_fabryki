//
// Created by Kubq on 2019-01-13.
//

#include <ReceiverPreferences.hpp>
#include <experimental/random>

ReceiverPreferences::ReceiverPreferences(std::map<IPackageReceiver*, ProbabilityGenerator>& receiverPreferences)
        : _receiverPreferences(receiverPreferences) {}



void ReceiverPreferences::scaleProbability(std::map<IPackageReceiver*, ProbabilityGenerator> receiverPreferences){
    ProbabilityGenerator sum_of_probabilities = 0;
    for(auto ele=receiverPreferences.cbegin(); ele!=receiverPreferences.cend();ele++ ){
        sum_of_probabilities=sum_of_probabilities+ele->second;
    }
    std::map<IPackageReceiver*, ProbabilityGenerator> new_pref;
    for(auto ele=receiverPreferences.begin(); ele!=receiverPreferences.end();ele++ ){
        new_pref.insert(std::pair<IPackageReceiver*, ProbabilityGenerator>(ele->first,ele->second/sum_of_probabilities));
    }
    _receiverPreferences=new_pref;
}


void ReceiverPreferences::addReceiver(IPackageReceiver* newReceiver,  ProbabilityGenerator probability) {
    _receiverPreferences.insert(std::pair<IPackageReceiver*, ProbabilityGenerator>(newReceiver, probability));
    scaleProbability(_receiverPreferences);
}


std::map<IPackageReceiver*, ProbabilityGenerator>::const_iterator ReceiverPreferences::begin() const { return _receiverPreferences.begin(); }
std::map<IPackageReceiver*, ProbabilityGenerator>::const_iterator ReceiverPreferences::end() const { return _receiverPreferences.end(); }
std::map<IPackageReceiver*, ProbabilityGenerator>::const_iterator ReceiverPreferences::cbegin() const { return _receiverPreferences.cbegin(); }
std::map<IPackageReceiver*, ProbabilityGenerator>::const_iterator ReceiverPreferences::cend() const { return _receiverPreferences.cend(); }

void ReceiverPreferences::removeReceiver(const ElementID& receiverID){
    for(auto ele=_receiverPreferences.begin(); ele!=_receiverPreferences.end(); ele++){
        if(ele->first->identifyReceiver().second==receiverID){
            _receiverPreferences.erase(ele);
            scaleProbability(_receiverPreferences);
        }
    }
}



double randDouble(){return (double) rand() / (RAND_MAX);}

double get_0_5() {
    return 0.5;
}


IPackageReceiver* ReceiverPreferences::selectReceiver(std::function<double()> function) const {
    double rand_select = function();
    double sum=0;
    if(rand_select==0){ return cbegin()->first;}
    for(auto ele=cbegin();ele!=cend();ele++){
        if(rand_select>sum && rand_select<=sum+ele->second){ return ele->first;}
        sum=sum+ele->second;
    }
}


