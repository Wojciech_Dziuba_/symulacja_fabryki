//
// Created by wojte on 12.01.2019.
//

#include <Ramp.hpp>
#include <Package.hpp>

Ramp::Ramp(ElementID rampID, TimeOffset delieveryTimes, ReceiverPreferences& preferences)
: PackageSender(preferences), _rampID(rampID), _delieveryTimes(delieveryTimes) {}

ElementID Ramp::getID() const { return _rampID; }

TimeOffset Ramp::getDeliveryTimes() const { return this->_delieveryTimes; }

void Ramp::tryGeneratePackage(const Time& currentTurn){
        if(currentTurn%this->getDeliveryTimes() == 0){
            setPackageToSendBuffer(Package());
    }
}