//
// Created by wojte on 12.01.2019.
//
#include <Worker.hpp>

Worker::Worker(ElementID workerID, TimeOffset workTime, IPackageQueue* workQueue, ReceiverPreferences& preferences)
: PackageSender(preferences), _workerID(workerID), _workTime(workTime), _workQueue(workQueue), _currentWorkTurn(0) {}

IPackageQueue* Worker::getWorkQueue() const { return _workQueue; }

std::optional<Package> Worker::getWorkBuffer() const { return _workBuffer; }

ElementID Worker::getID() const { return _workerID; }

void Worker::doWork() {
    if (_currentWorkTurn < _workTime && _workBuffer.has_value()) {
        _currentWorkTurn++;
    } else if (_currentWorkTurn < _workTime && !_workBuffer.has_value()) {
        _currentWorkTurn = 1;
        _workBuffer = *_workQueue->popPackage();
    } else if (_currentWorkTurn == _workTime && _workBuffer.has_value()) {
        _currentWorkTurn = 0;
        setPackageToSendBuffer(*_workBuffer);
        _workBuffer.reset();
        _workBuffer = *_workQueue->popPackage();
    } else {
        std::cout << "Something went wrong" << std::endl; // Error to be implemented
    }
}

void Worker::receivePackage(std::unique_ptr<Package>& packagePtr) const { _workQueue->addPackage(std::move(packagePtr)); }

std::pair<ReceiverType, ElementID > Worker::identifyReceiver() const {
    return std::pair<ReceiverType,ElementID >(ReceiverType::Worker, _workerID);
}

std::deque<Package>::const_iterator Worker::begin() const { return this->_workQueue->begin(); }
std::deque<Package>::const_iterator Worker::end() const { return this->_workQueue->end(); }
std::deque<Package>::const_iterator Worker::cbegin() const { return this->_workQueue->cbegin(); }
std::deque<Package>::const_iterator Worker::cend() const { return this->_workQueue->cend(); }
