//
// Created by Wojciech Dziuba (292639) on 18.01.2019.
//

#include <Storehouse.hpp>

Storehouse::Storehouse(ElementID storehouseID, IPackageStockpile* packageQueue)
    : _storehouseID(storehouseID), _packageQueue(packageQueue){}

ElementID Storehouse::getID() const { return _storehouseID; }

std::pair<ReceiverType, ElementID > Storehouse::identifyReceiver() const {
    return std::pair<ReceiverType,ElementID >(ReceiverType::Storehouse, _storehouseID);
}
void Storehouse::receivePackage(std::unique_ptr<Package>& packagePtr) const {
    this -> _packageQueue -> addPackage(std::move(packagePtr));
}

std::deque<Package>::const_iterator Storehouse::begin() const { return this -> _packageQueue -> begin(); }
std::deque<Package>::const_iterator Storehouse::end() const { return this -> _packageQueue -> end(); }
std::deque<Package>::const_iterator Storehouse::cbegin() const { return this -> _packageQueue -> cbegin(); }
std::deque<Package>::const_iterator Storehouse::cend() const { return this -> _packageQueue -> cend(); }