//
// Created by Wojciech Dziuba (292639) on 15.01.2019.
//

#include "gtest/gtest.h"

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}