//
// Created by Kubq on 2019-01-15.
//

#include "Package.hpp"
#include "PackageQueue.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "Globals.hpp"


TEST(PackageTest,getPackageID) {
    Package PackA;
    Package PackB;

    EXPECT_EQ(PackA.getPackageID(),1);
    EXPECT_EQ(PackB.getPackageID(),2);


}

TEST(PackageQueueTest, popPackageFIFO){
    Package PA;
    Package PB;
    Package PC;
    PackageQueue PQ(QueueType::FIFO);
    std::unique_ptr<Package> PackA_ptr=std::make_unique<Package> (PA);
    std::unique_ptr<Package> PackB_ptr=std::make_unique<Package> (PB);
    std::unique_ptr<Package> PackC_ptr=std::make_unique<Package> (PC);
    PQ.addPackage(std::move(PackA_ptr));
    PQ.addPackage(std::move(PackB_ptr));
    PQ.addPackage(std::move(PackC_ptr));
    std::unique_ptr<Package> popped=PQ.popPackage();
    int x=(*popped).getPackageID();
    EXPECT_EQ(x,3);
}

TEST(PackageQueueTest, popPackageLIFO){
    Package PA;
    Package PB;
    Package PC;
    PackageQueue PQ(QueueType::LIFO);
    std::unique_ptr<Package> PackA_ptr=std::make_unique<Package> (PA);
    std::unique_ptr<Package> PackB_ptr=std::make_unique<Package> (PB);
    std::unique_ptr<Package> PackC_ptr=std::make_unique<Package> (PC);
    PQ.addPackage(std::move(PackA_ptr));
    PQ.addPackage(std::move(PackB_ptr));
    PQ.addPackage(std::move(PackC_ptr));
    std::unique_ptr<Package> popped=PQ.popPackage();
    int x=(*popped).getPackageID();
    EXPECT_EQ(x,8);
}