//
// Created by Kubq on 2019-01-20.
//

#include "ReceiverPreferences.hpp"
#include "Storehouse.hpp"
#include "PackageQueue.hpp"
#include "Package.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "Globals.hpp"
#include "PackageSender.hpp"

class Mock_worker : public PackageSender, public IPackageReceiver{
private:
    IPackageQueue* _workQueue;
    ElementID _workerID;
public:
    Mock_worker(ElementID id, IPackageQueue* workQueue,ReceiverPreferences& preferences) : PackageSender(preferences) , _workQueue(workQueue), _workerID(id) {}
    IPackageQueue* getQueue() const {return _workQueue;}

    void receivePackage(std::unique_ptr<Package>& packagePtr) const { _workQueue->addPackage(std::move(packagePtr)); }

    std::pair<ReceiverType, ElementID > identifyReceiver() const {
        return std::pair<ReceiverType,ElementID >(ReceiverType::Worker, _workerID);}

    std::deque<Package>::const_iterator begin() const { return this->_workQueue->begin(); }
    std::deque<Package>::const_iterator end() const { return this->_workQueue->end(); }
    std::deque<Package>::const_iterator cbegin() const { return this->_workQueue->cbegin(); }
    std::deque<Package>::const_iterator cend() const { return this->_workQueue->cend(); }
};

TEST(PackageSenderTest, sendPackage){
    Package PackA;
    PackageQueue PQ1(QueueType::LIFO);
    std::unique_ptr<Package> PackA_ptr=std::make_unique<Package> (PackA);
    PQ1.addPackage(std::move(PackA_ptr));
    IPackageStockpile* PQ1_ptr=&PQ1;
    ElementID st1=1;
    Storehouse St1(st1,PQ1_ptr);
    Package PackB;
    //Package PackC;
    PackageQueue PQ2(QueueType::LIFO);
    std::unique_ptr<Package> PackB_ptr=std::make_unique<Package> (PackB);
    //std::unique_ptr<Package> PackC_ptr=std::make_unique<Package> (PackC);
    PQ2.addPackage(std::move(PackB_ptr));
    PackageQueue PQ3(QueueType::LIFO);
    //PQ3.addPackage(std::move(PackC_ptr));
    std::map<IPackageReceiver*, ProbabilityGenerator> worker1_pref;
    Storehouse* st_ptr=&St1;
    worker1_pref.insert(std::pair<IPackageReceiver*, ProbabilityGenerator>(st_ptr,1));
    ReceiverPreferences worker1_preferences(worker1_pref);
    ElementID wor1_id=1;
    TimeOffset wor1_time=2;
    IPackageQueue* PQ2_ptr=&PQ2;
    //std::unique_ptr<IPackageQueue> PQ3_ptr=std::make_unique<IPackageQueue>(PQ3);
    Mock_worker Worker1(wor1_id,PQ2_ptr, worker1_preferences);

    //Worker Worker2(wor1_id,wor1_time,std::move(PQ3_ptr), worker1_preferences);
    std::map<IPackageReceiver*,ProbabilityGenerator > r_pref;
    std::unique_ptr<Package> to_send;
    to_send=std::move(Worker1.getQueue()->popPackage());
    std::optional<Package> buffer = *(std::move(to_send));
    Worker1.setPackageToSendBuffer(buffer);
    Worker1.sendPackage();
    EXPECT_EQ(Worker1.getPackageToSendBuffer(),std::nullopt);


}