//
// Created by Wojciech Dziuba (292639) on 19.01.2019.
//
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <Factory.hpp>
#include <ReceiverPreferences.hpp>
#include <Ramp.hpp>
#include <Worker.hpp>
#include <Storehouse.hpp>
#include <PackageSender.hpp>
#include <PackageQueue.hpp>


TEST(FactoryTest, IsCoherent_Test) {
    // ramp_1
    std::map<IPackageReceiver*, ProbabilityGenerator> ramp_map_1;
    ReceiverPreferences ramp_1_preferences(ramp_map_1);
    Ramp ramp_1(ElementID(11), TimeOffset(1), ramp_1_preferences);

    // ramp_2
    std::map<IPackageReceiver*, ProbabilityGenerator> ramp_map_2;
    ReceiverPreferences ramp_2_preferences(ramp_map_2);
    Ramp ramp_2(ElementID(12), TimeOffset(1), ramp_2_preferences);

    // ramp_3
    std::map<IPackageReceiver*, ProbabilityGenerator> ramp_map_3;
    ReceiverPreferences ramp_3_preferences(ramp_map_3);
    Ramp ramp_3(ElementID(13), TimeOffset(1), ramp_3_preferences);

    // worker_1
    std::map<IPackageReceiver*, ProbabilityGenerator> worker_map_1;
    ReceiverPreferences worker_1_preferences(worker_map_1);
    PackageQueue worker_1_queue(QueueType::LIFO);
    IPackageQueue* worker_1_queue_ptr = &worker_1_queue;
    Worker worker_1(ElementID(21), TimeOffset(1), worker_1_queue_ptr, worker_1_preferences);

    // worker_2
    std::map<IPackageReceiver*, ProbabilityGenerator> worker_map_2;
    ReceiverPreferences worker_2_preferences(worker_map_2);
    PackageQueue worker_2_queue(QueueType::LIFO);
    IPackageQueue* worker_2_queue_ptr = &worker_2_queue;
    Worker worker_2(ElementID(22), TimeOffset(1), worker_2_queue_ptr, worker_2_preferences);

    // storehouse_1
    std::map<IPackageReceiver*, ProbabilityGenerator> storehouse_map_1;
    ReceiverPreferences storehouse_1_preferences(storehouse_map_1);
    PackageQueue storehouse_1_queue(QueueType::LIFO);
    IPackageStockpile* storehouse_1_queue_ptr = &storehouse_1_queue;
    Storehouse storehouse_1(ElementID(31), storehouse_1_queue_ptr);

    ramp_1.receiverPreferences.addReceiver(&worker_1, 0.5);
    ramp_1.receiverPreferences.addReceiver(&storehouse_1, 0.5);

    ramp_2.receiverPreferences.addReceiver(&worker_1, 1);

    ramp_3.receiverPreferences.addReceiver(&worker_2, 1);

    worker_1.receiverPreferences.addReceiver(&worker_2, 1);

    worker_2.receiverPreferences.addReceiver(&storehouse_1, 1);

    std::list<Ramp> ramp_list {ramp_1, ramp_2, ramp_3};
    Ramps ramps(ramp_list);

    std::list<Worker> worker_list {worker_1, worker_2};
    Workers workers(worker_list);

    std::list<Storehouse> storehouse_list {storehouse_1};
    Storehouses storehouses(storehouse_list);

    Factory factory(ramps, workers, storehouses);

    EXPECT_TRUE(factory.isCoherent());
}

TEST(FactoryTest, IsNotCoherent_Test) {
    // ramp_1
    std::map<IPackageReceiver*, ProbabilityGenerator> ramp_map_1;
    ReceiverPreferences ramp_1_preferences(ramp_map_1);
    Ramp ramp_1(ElementID(11), TimeOffset(1), ramp_1_preferences);

    // ramp_2
    std::map<IPackageReceiver*, ProbabilityGenerator> ramp_map_2;
    ReceiverPreferences ramp_2_preferences(ramp_map_2);
    Ramp ramp_2(ElementID(12), TimeOffset(1), ramp_2_preferences);

    // ramp_3
    std::map<IPackageReceiver*, ProbabilityGenerator> ramp_map_3;
    ReceiverPreferences ramp_3_preferences(ramp_map_3);
    Ramp ramp_3(ElementID(13), TimeOffset(1), ramp_3_preferences);

    // worker_1
    std::map<IPackageReceiver*, ProbabilityGenerator> worker_map_1;
    ReceiverPreferences worker_1_preferences(worker_map_1);
    PackageQueue worker_1_queue(QueueType::LIFO);
    IPackageQueue* worker_1_queue_ptr = &worker_1_queue;
    Worker worker_1(ElementID(21), TimeOffset(1), worker_1_queue_ptr, worker_1_preferences);

    // worker_2
    std::map<IPackageReceiver*, ProbabilityGenerator> worker_map_2;
    ReceiverPreferences worker_2_preferences(worker_map_2);
    PackageQueue worker_2_queue(QueueType::LIFO);
    IPackageQueue* worker_2_queue_ptr = &worker_2_queue;
    Worker worker_2(ElementID(22), TimeOffset(1), worker_2_queue_ptr, worker_2_preferences);

    // storehouse_1
    std::map<IPackageReceiver*, ProbabilityGenerator> storehouse_map_1;
    ReceiverPreferences storehouse_1_preferences(storehouse_map_1);
    PackageQueue storehouse_1_queue(QueueType::LIFO);
    IPackageStockpile* storehouse_1_queue_ptr = &storehouse_1_queue;
    Storehouse storehouse_1(ElementID(31), storehouse_1_queue_ptr);

    ramp_1.receiverPreferences.addReceiver(&worker_1, 0.5);
    ramp_1.receiverPreferences.addReceiver(&worker_2, 0.5);

    ramp_2.receiverPreferences.addReceiver(&worker_2, 1);

    ramp_3.receiverPreferences.addReceiver(&storehouse_1, 1);

    //worker_1 has no outgoing connections and should cause incoherency

    worker_2.receiverPreferences.addReceiver(&storehouse_1, 1);

    std::list<Ramp> ramp_list {ramp_1, ramp_2};
    Ramps ramps(ramp_list);

    std::list<Worker> worker_list {worker_1, worker_2};
    Workers workers(worker_list);

    std::list<Storehouse> storehouses_list {storehouse_1};
    Storehouses storehouses(storehouses_list);

    Factory factory(ramps, workers, storehouses);

    EXPECT_FALSE(factory.isCoherent());
}

TEST(FactoryTest, DeletingByID){
    // ramp_1
    std::map<IPackageReceiver*, ProbabilityGenerator> ramp_map_1;
    ReceiverPreferences ramp_1_preferences(ramp_map_1);
    Ramp ramp_1(ElementID(11), TimeOffset(1), ramp_1_preferences);

    // ramp_2
    std::map<IPackageReceiver*, ProbabilityGenerator> ramp_map_2;
    ReceiverPreferences ramp_2_preferences(ramp_map_2);
    Ramp ramp_2(ElementID(12), TimeOffset(1), ramp_2_preferences);

    // ramp_3
    std::map<IPackageReceiver*, ProbabilityGenerator> ramp_map_3;
    ReceiverPreferences ramp_3_preferences(ramp_map_3);
    Ramp ramp_3(ElementID(13), TimeOffset(1), ramp_3_preferences);

    // worker_1
    std::map<IPackageReceiver*, ProbabilityGenerator> worker_map_1;
    ReceiverPreferences worker_1_preferences(worker_map_1);
    PackageQueue worker_1_queue(QueueType::LIFO);
    IPackageQueue* worker_1_queue_ptr = &worker_1_queue;
    Worker worker_1(ElementID(21), TimeOffset(1), worker_1_queue_ptr, worker_1_preferences);

    // worker_2
    std::map<IPackageReceiver*, ProbabilityGenerator> worker_map_2;
    ReceiverPreferences worker_2_preferences(worker_map_2);
    PackageQueue worker_2_queue(QueueType::LIFO);
    IPackageQueue* worker_2_queue_ptr = &worker_2_queue;
    Worker worker_2(ElementID(22), TimeOffset(1), worker_2_queue_ptr, worker_2_preferences);

    // storehouse_1
    std::map<IPackageReceiver*, ProbabilityGenerator> storehouse_map_1;
    ReceiverPreferences storehouse_1_preferences(storehouse_map_1);
    PackageQueue storehouse_1_queue(QueueType::LIFO);
    IPackageStockpile* storehouse_1_queue_ptr = &storehouse_1_queue;
    Storehouse storehouse_1(ElementID(31), storehouse_1_queue_ptr);

    ramp_1.receiverPreferences.addReceiver(&worker_1, 0.5);
    ramp_1.receiverPreferences.addReceiver(&storehouse_1, 0.5);

    ramp_2.receiverPreferences.addReceiver(&worker_1, 1);

    ramp_3.receiverPreferences.addReceiver(&worker_2, 1);

    worker_1.receiverPreferences.addReceiver(&worker_2, 1);

    worker_2.receiverPreferences.addReceiver(&storehouse_1, 1);

    std::list<Ramp> ramp_list {ramp_1, ramp_2, ramp_3};
    Ramps ramps(ramp_list);

    std::list<Worker> worker_list {worker_1, worker_2};
    Workers workers(worker_list);

    std::list<Storehouse> storehouse_list {storehouse_1};
    Storehouses storehouses(storehouse_list);

    Factory factory(ramps, workers, storehouses);
    EXPECT_TRUE(factory.isCoherent());

    factory.removeStorehouseByID(31);
    EXPECT_FALSE(factory.isCoherent());

}