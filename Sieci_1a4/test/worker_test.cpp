//
// Created by Wojciech Dziuba (292639) on 19.01.2019.
//
#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "PackageQueue.hpp"

class Test_Package{
private:
    ElementID _packageID;
public:
    explicit Test_Package(ElementID packageID) : _packageID(packageID) {}
    ElementID getPackageID() const { return _packageID; }
};

enum class Test_QueueType {
    FIFO,
    LIFO
};

class Test_PackageSender {
private:
    std::optional<Test_Package> _packageToSendBuffer;
public:
    // Unnecesery in the test scenario
    // ReceiverPreferences preferences;

    Test_PackageSender() = default;

    std::optional<Test_Package> getPackageToSendBuffer() const { return this->_packageToSendBuffer; };
    void setPackageToSendBuffer(const std::optional<Test_Package>& packageToSend) { _packageToSendBuffer = packageToSend; }

    // Simulates effects of sending package for PackageSender
    // and its inheritors by simply emptying _packageToSendBuffer
    void sendPackage() { this -> _packageToSendBuffer.reset(); }
};

class Test_IPackageReceiver {
public:
    virtual void receivePackage(std::unique_ptr<Test_Package> packagePtr) = 0;

    // Unnecesery in the test scenario
    //virtual void identyfyReceiver() = 0;

    virtual ~Test_IPackageReceiver() = default;
};

class Test_IPackageStockpile {
public:
    virtual void addPackage(std::unique_ptr<Test_Package> testPackage) = 0;
    //virtual std::deque<Test_Package>::const_iterator getPackageByID(ElementID packageID) = 0; // Unncecesery for test scenario
    virtual ~Test_IPackageStockpile() = default;
};

class Test_IPackageQueue : public Test_IPackageStockpile{
public:
    //virtual void removePackage(ElementID packageID) = 0; // Unnecessary for test scenario
    virtual std::unique_ptr<Test_Package> popPackage() =0;
    virtual Test_QueueType identyfyQueue() const = 0;
    virtual ~Test_IPackageQueue() = default;
};

class Test_PackageQueue : public Test_IPackageQueue{
private:
    std::deque<Test_Package> _queue;
    Test_QueueType _type;
public:
    explicit Test_PackageQueue(Test_QueueType type) : _type(type) {}

    void addPackage(std::unique_ptr<Test_Package> testPackage) override {
        switch(_type) {
            case Test_QueueType::LIFO:
                _queue.push_back(*std::move(testPackage));
                break;
            case Test_QueueType::FIFO:
                _queue.push_front(*std::move(testPackage));
                break;
        }
    }

    std::unique_ptr<Test_Package> popPackage() override {
        if(_type == Test_QueueType::FIFO) {
            std::unique_ptr<Test_Package> packagePtr = std::make_unique<Test_Package>(_queue.front());
            _queue.pop_front();
            return packagePtr;
        }else if(_type == Test_QueueType::LIFO){
            std::unique_ptr<Test_Package> packagePtr= std::make_unique<Test_Package>(_queue.front());
            _queue.pop_front();
            return packagePtr;
        }
    }
    Test_QueueType identyfyQueue() const override { return this->_type; }
};

class Test_Worker : public Test_PackageSender, public Test_IPackageReceiver {
private:
    //ElementID _workerID; // Unnecessary for test scenario
    TimeOffset _workTime;
    Time _currentWorkTurn;
    std::optional<Test_Package> _workBuffer;
    Test_IPackageQueue* _workQueue;
public:
    Test_Worker(TimeOffset workTime, Test_IPackageQueue* workQueue)
    : _workTime(workTime), _workQueue(workQueue) { _currentWorkTurn = 0; }

    Test_IPackageQueue* getWorkQueue() const {
        return _workQueue;
    }
    std::optional<Test_Package> getWorkBuffer() const {
        return _workBuffer;
    }

    void doWork() {
        if (_currentWorkTurn < _workTime && _workBuffer.has_value()) {
            _currentWorkTurn++;
        } else if (_currentWorkTurn < _workTime && !_workBuffer.has_value()) {
            _currentWorkTurn = 1;
            _workBuffer = *_workQueue->popPackage();
        } else if (_currentWorkTurn == _workTime && _workBuffer.has_value()) {
            _currentWorkTurn = 0;
            setPackageToSendBuffer(_workBuffer);
            _workBuffer.reset();
            _workBuffer = *_workQueue->popPackage();
        } else {
            std::cout << "Something went wrong" << std::endl; // Error to be implemented
        }
    }

    void receivePackage(std::unique_ptr<Test_Package> packagePtr) override { _workQueue->addPackage(std::move(packagePtr)); }
};

TEST(WorkerTest, RecievingPackage_Test_LIFO){

    Test_PackageQueue queue(Test_QueueType::LIFO);
    Test_IPackageQueue* queue_ptr = &queue;
    Test_Worker test_worker(TimeOffset(2), queue_ptr);

    Test_Package package_1(1);
    Test_Package package_2(2);
    Test_Package package_3(3);
    Test_Package package_4(4);
    test_worker.receivePackage(std::make_unique<Test_Package>(package_1));
    test_worker.receivePackage(std::make_unique<Test_Package>(package_2));
    test_worker.receivePackage(std::make_unique<Test_Package>(package_3));
    test_worker.receivePackage(std::make_unique<Test_Package>(package_4));

    EXPECT_EQ(test_worker.getWorkQueue()->popPackage()->getPackageID(), package_1.getPackageID());
}

TEST(WorkerTest, RecievingPackage_Test_FIFO){

    Test_PackageQueue queue(Test_QueueType::FIFO);
    Test_IPackageQueue* queue_ptr = &queue;
    Test_Worker test_worker(TimeOffset(2), queue_ptr);

    Test_Package package_1(1);
    Test_Package package_2(2);
    Test_Package package_3(3);
    Test_Package package_4(4);
    test_worker.receivePackage(std::make_unique<Test_Package>(package_1));
    test_worker.receivePackage(std::make_unique<Test_Package>(package_2));
    test_worker.receivePackage(std::make_unique<Test_Package>(package_3));
    test_worker.receivePackage(std::make_unique<Test_Package>(package_4));

    EXPECT_EQ(test_worker.getWorkQueue()->popPackage()->getPackageID(), package_4.getPackageID());
}

TEST(WorkerTest, DoingWork_Test_WorkTurns){

    Test_PackageQueue queue(Test_QueueType::LIFO);
    Test_IPackageQueue* queue_ptr = &queue;
    Test_Worker test_worker(TimeOffset(2), queue_ptr);

    Test_Package package_1(1);
    Test_Package package_2(2);
    Test_Package package_3(3);
    Test_Package package_4(4);
    test_worker.receivePackage(std::make_unique<Test_Package>(package_1));
    test_worker.receivePackage(std::make_unique<Test_Package>(package_2));
    test_worker.receivePackage(std::make_unique<Test_Package>(package_3));
    test_worker.receivePackage(std::make_unique<Test_Package>(package_4));

    EXPECT_FALSE(test_worker.getWorkBuffer().has_value());

    test_worker.doWork();
    EXPECT_EQ(test_worker.getWorkBuffer()->getPackageID(), package_1.getPackageID());

    test_worker.doWork();
    EXPECT_EQ(test_worker.getWorkBuffer()->getPackageID(), package_1.getPackageID());

    test_worker.doWork();
    EXPECT_EQ(test_worker.getPackageToSendBuffer()->getPackageID(), package_1.getPackageID());
    EXPECT_EQ(test_worker.getWorkBuffer()->getPackageID(), package_2.getPackageID());
}