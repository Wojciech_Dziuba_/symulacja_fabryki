//
// Created by Wojciech Dziuba (292639) on 15.01.2019.
//


#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "Globals.hpp"

class TestPackage {
};

class TestPackageSender {
private:
    std::optional<TestPackage> _packageToSendBuffer;
public:
    TestPackageSender() = default;
    std::optional<TestPackage> getPackageToSendBuffer() const { return this->_packageToSendBuffer; };
    void setPackageToSendBuffer(const std::optional<TestPackage>& packageToSend) { _packageToSendBuffer = packageToSend; }

    // Simulates effects of sending package for PackageSender and its inheritors by simply emptying _packageToSendBuffer
    void sendPackage() { this -> _packageToSendBuffer.reset(); }
};

class TestRamp : public TestPackageSender {
private:
    TimeOffset _deliveryTimes;
public:
    explicit TestRamp(TimeOffset deliveryTimes) : _deliveryTimes(deliveryTimes) {}
    TimeOffset getDeliveryTimes() const { return this->_deliveryTimes; }
    void tryGeneratePackage(const Time& currentTurn) {
        if (currentTurn % this->getDeliveryTimes() == 0) {
            this->setPackageToSendBuffer(TestPackage());
        }
    }
};

TEST(RampTest, IsProductInstatntlyInBuffor) {
    TestRamp test_ramp(3);
    EXPECT_FALSE(test_ramp.getPackageToSendBuffer().has_value());

    test_ramp.tryGeneratePackage(3);
    EXPECT_TRUE(test_ramp.getPackageToSendBuffer().has_value());

    test_ramp.sendPackage();
    EXPECT_FALSE(test_ramp.getPackageToSendBuffer().has_value());

    test_ramp.tryGeneratePackage(6);
    EXPECT_TRUE(test_ramp.getPackageToSendBuffer().has_value());
}

TEST(RampTest, IsDelieveryInCorrectTurn) {
    TestRamp test_ramp(3);

    EXPECT_FALSE(test_ramp.getPackageToSendBuffer().has_value());

    test_ramp.tryGeneratePackage(1);
    EXPECT_FALSE(test_ramp.getPackageToSendBuffer().has_value());
    test_ramp.sendPackage();

    test_ramp.tryGeneratePackage(2);
    EXPECT_FALSE(test_ramp.getPackageToSendBuffer().has_value());
    test_ramp.sendPackage();

    test_ramp.tryGeneratePackage(3);
    EXPECT_TRUE(test_ramp.getPackageToSendBuffer().has_value());
    test_ramp.sendPackage();

    test_ramp.tryGeneratePackage(4);
    EXPECT_FALSE(test_ramp.getPackageToSendBuffer().has_value());
    test_ramp.sendPackage();

    test_ramp.tryGeneratePackage(5);
    EXPECT_FALSE(test_ramp.getPackageToSendBuffer().has_value());
    test_ramp.sendPackage();

    test_ramp.tryGeneratePackage(6);
    EXPECT_TRUE(test_ramp.getPackageToSendBuffer().has_value());
    test_ramp.sendPackage();

    test_ramp.tryGeneratePackage(7);
    EXPECT_FALSE(test_ramp.getPackageToSendBuffer().has_value());
    test_ramp.sendPackage();
}