//
// Created by Wojciech Dziuba (292639) on 21.01.2019.
//

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <NodeCollection.hpp>
#include <ReceiverPreferences.hpp>
#include <PackageSender.hpp>
#include <Ramp.hpp>




TEST(NodeCollectionTest, FindNodeByID){

    // ramp_1
    std::map<IPackageReceiver*, ProbabilityGenerator> ramp_map_1;
    ReceiverPreferences ramp_1_preferences(ramp_map_1);
    Ramp ramp_1(ElementID(11), TimeOffset(1), ramp_1_preferences);

    // ramp_2
    std::map<IPackageReceiver*, ProbabilityGenerator> ramp_map_2;
    ReceiverPreferences ramp_2_preferences(ramp_map_2);
    Ramp ramp_2(ElementID(12), TimeOffset(1), ramp_2_preferences);

    std::list<Ramp> ramp_list {ramp_1, ramp_2};
    Ramps ramps(ramp_list);

    EXPECT_EQ(ramps.getNodeByID(12)->getID(), 12);
    EXPECT_EQ(ramps.getNodeByID(11)->getID(), 11);
}

TEST(NodeCollectionTest, RemoveNodeByID){

    // ramp_1
    std::map<IPackageReceiver*, ProbabilityGenerator> ramp_map_1;
    ReceiverPreferences ramp_1_preferences(ramp_map_1);
    Ramp ramp_1(ElementID(11), TimeOffset(1), ramp_1_preferences);

    // ramp_2
    std::map<IPackageReceiver*, ProbabilityGenerator> ramp_map_2;
    ReceiverPreferences ramp_2_preferences(ramp_map_2);
    Ramp ramp_2(ElementID(12), TimeOffset(1), ramp_2_preferences);

    std::list<Ramp> ramp_list {ramp_1, ramp_2};
    Ramps ramps(ramp_list);

    EXPECT_EQ(ramps.getNodeByID(11)->getID(), 11);
    EXPECT_EQ(ramps.getNodeByID(12)->getID(), 12);

    ramps.removeNodeByID(11);
    EXPECT_EQ(ramps.getNodeByID(11), ramps.end());
    EXPECT_EQ(ramps.getNodeByID(12)->getID(), 12);
}
