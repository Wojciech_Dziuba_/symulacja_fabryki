//
// Created by Kubq on 2019-01-20.
//

#include "ReceiverPreferences.hpp"
#include "Storehouse.hpp"
#include "PackageQueue.hpp"
#include "Package.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "Globals.hpp"



TEST(ReceiverPreferencesTest,addReceiver){
    Package PackA;
    Package PackB;
    Package PackC;
    PackageQueue PQ1(QueueType::LIFO);
    PackageQueue PQ2(QueueType::LIFO);
    PackageQueue PQ3(QueueType::LIFO);
    std::unique_ptr<Package> PackA_ptr=std::make_unique<Package> (PackA);
    std::unique_ptr<Package> PackB_ptr=std::make_unique<Package> (PackB);
    std::unique_ptr<Package> PackC_ptr=std::make_unique<Package> (PackC);
    PQ1.addPackage(std::move(PackA_ptr));
    PQ2.addPackage(std::move(PackB_ptr));
    PQ3.addPackage(std::move(PackC_ptr));
    IPackageStockpile* PQ1_ptr=&PQ1;
    ElementID st1=1;
    Storehouse St1(st1,PQ1_ptr);
    IPackageStockpile* PQ2_ptr=&PQ2;
    ElementID st2=2;
    Storehouse St2(st2,PQ2_ptr);
    IPackageStockpile* PQ3_ptr=&PQ3;
    ElementID st3=3;
    Storehouse St3(st3,PQ3_ptr);
    std::map<IPackageReceiver*,ProbabilityGenerator > prefer;
    Storehouse* st1_ptr=&St1;
    Storehouse* st2_ptr=&St2;
    prefer.insert(std::pair<IPackageReceiver*,ProbabilityGenerator>(st1_ptr,0.4));
    prefer.insert(std::pair<IPackageReceiver*,ProbabilityGenerator>(st2_ptr,0.6));
    ReceiverPreferences preferences(prefer);
    Storehouse* st3_ptr=&St3;
    preferences.addReceiver(st3_ptr,0.6);
    ProbabilityGenerator sum=0;
    ProbabilityGenerator x=1.000000;

    for(auto ele=preferences.cbegin(); ele!=preferences.cend();ele++ ){
        sum=sum+ele->second;
    }
    if(sum>0.999999999) {
        sum = 1;
    }
    EXPECT_EQ(sum,x);

}

TEST(ReceiverPreferencesTest,removeReceiver) {
    Package PackA;
    Package PackB;
    Package PackC;
    PackageQueue PQ1(QueueType::LIFO);
    PackageQueue PQ2(QueueType::LIFO);
    PackageQueue PQ3(QueueType::LIFO);
    std::unique_ptr<Package> PackA_ptr=std::make_unique<Package> (PackA);
    std::unique_ptr<Package> PackB_ptr=std::make_unique<Package> (PackB);
    std::unique_ptr<Package> PackC_ptr=std::make_unique<Package> (PackC);
    PQ1.addPackage(std::move(PackA_ptr));
    PQ2.addPackage(std::move(PackB_ptr));
    PQ3.addPackage(std::move(PackC_ptr));
    IPackageStockpile* PQ1_ptr=&PQ1;
    ElementID st1 = 1;
    Storehouse St1(st1, PQ1_ptr);
    IPackageStockpile* PQ2_ptr=&PQ2;
    ElementID st2 = 2;
    Storehouse St2(st2, PQ2_ptr);
    IPackageStockpile* PQ3_ptr=&PQ3;
    ElementID st3 = 3;
    Storehouse St3(st3, PQ3_ptr);
    std::map<IPackageReceiver *, ProbabilityGenerator> prefer;
    Storehouse *st1_ptr = &St1;
    Storehouse *st2_ptr = &St2;
    Storehouse* st3_ptr=&St3;
    prefer.insert(std::pair<IPackageReceiver*,ProbabilityGenerator>(st1_ptr,0.25));
    prefer.insert(std::pair<IPackageReceiver*,ProbabilityGenerator>(st2_ptr,0.375));
    prefer.insert(std::pair<IPackageReceiver*,ProbabilityGenerator>(st3_ptr,0.375));
    ReceiverPreferences preferences(prefer);
    preferences.removeReceiver(st3);
    ProbabilityGenerator sum=0;
    ProbabilityGenerator x=1;
    for(auto ele=preferences.cbegin(); ele!=preferences.cend();ele++ ){
        sum=sum+ele->second;
        std::cout<<ele->second<<std::endl;
    }
    if(sum>0.999999999){
        sum=1;
    }
    EXPECT_EQ(sum,x);

}

TEST(ReceiverPreferencesTest,selectReceiver) {
    Package PackA;
    Package PackB;
    Package PackC;
    PackageQueue PQ1(QueueType::LIFO);
    PackageQueue PQ2(QueueType::LIFO);
    PackageQueue PQ3(QueueType::LIFO);
    std::unique_ptr<Package> PackA_ptr=std::make_unique<Package> (PackA);
    std::unique_ptr<Package> PackB_ptr=std::make_unique<Package> (PackB);
    std::unique_ptr<Package> PackC_ptr=std::make_unique<Package> (PackC);
    PQ1.addPackage(std::move(PackA_ptr));
    PQ2.addPackage(std::move(PackB_ptr));
    PQ3.addPackage(std::move(PackC_ptr));
    IPackageStockpile* PQ1_ptr=&PQ1;
    ElementID st1 = 1;
    Storehouse St1(st1, PQ1_ptr);
    IPackageStockpile* PQ2_ptr=&PQ2;
    ElementID st2 = 2;
    Storehouse St2(st2, PQ2_ptr);
    IPackageStockpile* PQ3_ptr=&PQ3;
    ElementID st3 = 3;
    Storehouse St3(st3, PQ3_ptr);
    std::map<IPackageReceiver *, ProbabilityGenerator> prefer;
    Storehouse *st1_ptr = &St1;
    Storehouse *st2_ptr = &St2;
    Storehouse* st3_ptr=&St3;
    prefer.insert(std::pair<IPackageReceiver*,ProbabilityGenerator>(st1_ptr,0.25));
    prefer.insert(std::pair<IPackageReceiver*,ProbabilityGenerator>(st2_ptr,0.375));
    prefer.insert(std::pair<IPackageReceiver*,ProbabilityGenerator>(st3_ptr,0.375));
    ReceiverPreferences preferences(prefer);
    std::function<double()> fun=get_0_5;
    IPackageReceiver* selected=preferences.selectReceiver(fun);
    ElementID id=selected->identifyReceiver().second;
    ElementID x=2;
    EXPECT_EQ(id,x);


}