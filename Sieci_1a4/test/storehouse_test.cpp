//
// Created by Wojciech Dziuba (292639) on 19.01.2019.
//
#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "Globals.hpp"

class TestPackage {
private:
    ElementID _id;
public:
    TestPackage(ElementID id) : _id(id) {}
    ElementID getID() const { return _id; }
};


enum class TestQueueType {
    FIFO,
    LIFO
};


class TestIPackageStockpile {
public:
    virtual void addPackage(std::unique_ptr<TestPackage> testPackage) = 0;
    virtual std::unique_ptr<TestPackage> getPackage() = 0;
    virtual ~TestIPackageStockpile() = default;
};


class TestIPackageQueue : public TestIPackageStockpile{
public:
    virtual TestQueueType identyfyQueue() const = 0;
    //virtual std::unique_ptr<TestPackage> getPackage() = 0;
    virtual ~TestIPackageQueue() = default;
};


class TestPackageQueue : public TestIPackageQueue{
private:
    std::deque<TestPackage> _queue;
    TestQueueType _type;
public:
    explicit TestPackageQueue(TestQueueType type) : _type(type) {}

    void addPackage(std::unique_ptr<TestPackage> testPackage) override {
        switch(_type) {
            case TestQueueType::LIFO:
                _queue.push_back(*std::move(testPackage));
                break;
            case TestQueueType::FIFO:
                _queue.push_front(*std::move(testPackage));
                break;
        }
    }

    std::unique_ptr<TestPackage> getPackage() override {
        if(_type == TestQueueType::FIFO) {
            std::unique_ptr<TestPackage> packagePtr = std::make_unique<TestPackage>(_queue.front());
            _queue.pop_front();
            return packagePtr;
        }else if(_type == TestQueueType::LIFO){
            std::unique_ptr<TestPackage> packagePtr= std::make_unique<TestPackage>(_queue.front());
            _queue.pop_front();
            return packagePtr;
        }
    }

    TestQueueType identyfyQueue() const override { return this->_type; }
};


class TestIPackageReceiver {
public:
    virtual void receivePackage(std::unique_ptr<TestPackage> Package) = 0;
    virtual ~TestIPackageReceiver() = default;
};


class TestStorehouse : public TestIPackageReceiver{
private:
    TestIPackageStockpile* _packageQueue;
public:
    explicit TestStorehouse() {
        _packageQueue = new TestPackageQueue(TestQueueType::LIFO);
    }

    void receivePackage(std::unique_ptr<TestPackage> package) override {
        this->_packageQueue->addPackage(std::move(package));
    }

    TestIPackageStockpile* getPackageQueue() const { return _packageQueue; }
};

TEST(StorehouseTest, StorehouseTest_PacketRecieving_Test){
    TestStorehouse storehouse;

    TestPackage pack_1(1);
    TestPackage pack_2(2);
    TestPackage pack_3(3);

    storehouse.receivePackage(std::make_unique<TestPackage>(pack_1));
    storehouse.receivePackage(std::make_unique<TestPackage>(pack_2));
    storehouse.receivePackage(std::make_unique<TestPackage>(pack_3));

    EXPECT_EQ(storehouse.getPackageQueue()->getPackage()->getID(), pack_1.getID());
}
